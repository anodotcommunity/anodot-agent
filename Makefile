PYTHON=python3.6
MYPY=mypy

tests:
	#$(MYPY) --ignore-missing-imports .
	$(PYTHON) -m unittest

anodot-agent.tar: FORCE
	git archive --prefix=anodot-agent/ HEAD > anodot-agent.tar

release: anodot-agent.tar
	./scripts/build_release_docker.sh

clean:
	rm -f anodot-agent.tar
	docker images anodot/anodot-agent --format 'anodot/anodot-agent:{{.Tag}}' | xargs docker rmi --force=true dummy_placeholder

FORCE:


