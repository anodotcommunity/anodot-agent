import io
import unittest
import yaml

from anodot_agent.config import *

class DummySourceTestCase(unittest.TestCase):
    maxDiff = None
    def test_dummy_source(self):
        from anodot_agent.sources.dummy import Source
        from datetime import datetime
        source = Source(properties = ["column1","column2"])
        rs = source.get_chunk(start_dttm = datetime(2018,1,1), end_dttm = datetime(2018,1,1,0,1))
        self.assertEqual(rs.columns, ["timestamp", "column1", "column2"])
        self.assertEqual(rs.records, 
                [
                    (datetime(2018, 1, 1, 0, 0, 11, 630125), 0.6387190910098384, 0.3516388643543017), 
                    (datetime(2018, 1, 1, 0, 0, 12, 701222), 0.6684542608519385, 0.44977098793164183), 
                    (datetime(2018, 1, 1, 0, 0, 19, 207748), 0.38635023385308176, 0.47729571189940223), 
                    (datetime(2018, 1, 1, 0, 0, 27, 377663), 0.9480929387952277, 0.5954559332661964), 
                    (datetime(2018, 1, 1, 0, 0, 39, 449421), 0.1809617080102548, 0.7269474520171096)
                ]
        )

if __name__ == '__main__':
    unittest.main()
