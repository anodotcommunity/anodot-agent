import io
import unittest
import yaml

from anodot_agent.config import *

class StreamParsingTestCase(unittest.TestCase):
    CONFIGS = [ {
            'original': '''
source:
  type: anodot_agent.sources.sql:SQLSource
  dburl: mysql://localhost:5432/app

destination:
  type: anodot_agent.destinations.s3csv:S3CSVDestination
  bucket: bucket_for_anodot

scheduler:
  interval: 1h

streams:
- id: clicks

  source.query:
    SELECT * from app_clicks
    WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

- id: conversions
  source.query:
    SELECT * from app_conversions
    WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

  scheduler:
    interval: 1d

- id: conversions_other
  source.query:
    SELECT * from app_conversions_other
    WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

  scheduler.interval: 4d

          ''',

          'flat': '''
- id: clicks
  source:
    type: anodot_agent.sources.sql:SQLSource
    dburl: mysql://localhost:5432/app
    query:
          SELECT * from app_clicks
          WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

  scheduler:
    interval: 1h
       
  destination:
    type: anodot_agent.destinations.s3csv:S3CSVDestination
    bucket: bucket_for_anodot
     
- id: conversions
  source:
    type: anodot_agent.sources.sql:SQLSource
    dburl: mysql://localhost:5432/app
    query:
          SELECT * from app_conversions
          WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

  destination:
    type: anodot_agent.destinations.s3csv:S3CSVDestination
    bucket: bucket_for_anodot
       
  scheduler:
    interval: 1d

- id: conversions_other
  source:
    type: anodot_agent.sources.sql:SQLSource
    dburl: mysql://localhost:5432/app
    query:
          SELECT * from app_conversions_other
          WHERE click_time >= '{{start_time}}' AND click_time < '{{end_time}}'

  destination:
    type: anodot_agent.destinations.s3csv:S3CSVDestination
    bucket: bucket_for_anodot
       
  scheduler:
    interval: 4d

    ''' } ]

    maxDiff = None

    def test_flatten(self):
        for conf in self.CONFIGS:
            original = yaml.load(io.StringIO(conf['original']))
            flat = yaml.load(io.StringIO(conf['flat']))
            self.assertEqual(list(flatten_stream_configs(original)), flat)


    def test_validates_has_id(self):
        with self.assertRaises(MissingRequiredKeyError):
            a = list(flatten_stream_configs(yaml.load(io.StringIO('''
streams:
  - scheduler:
      interval: 1h
      
    destination:
      type: anodot_agent.destinations.s3csv:S3CSVDestination
      bucket: my_bucket
    
    source:
      type: anodot_agent.sources.sql:SQLSource
      dburl: mysql://localhost:5432/app
      '''))))

    def test_validates_unique_ids(self):
        with self.assertRaises(DuplicateIdError):
            a = list(flatten_stream_configs(yaml.load(io.StringIO('''
scheduler:
  interval: 1h
  
destination:
  type: anodot_agent.destinations.s3csv:S3CSVDestination
  bucket: my_bucket

source:
  type: anodot_agent.sources.sql:SQLSource
  dburl: mysql://localhost:5432/app

streams:
  - id: a
  - id: b
  - id: a
            '''))))
            validate_stream_configs(a)



if __name__ == '__main__':
    unittest.main()
