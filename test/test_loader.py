import io
import unittest
import yaml
from anodot_agent.sources.base import BaseSource
from anodot_agent.loader import source_loader, destination_loader, LoaderError

class DestinationSendingTestCase(unittest.TestCase):
    def test_load_source(self):
        from anodot_agent.sources.dummy import Source

        src = source_loader.load_class(config = {'handler': 'anodot_agent.sources.dummy'})
        self.assertEqual(src, Source)

    def test_load_destination(self):
        from anodot_agent.destinations.dummy import Destination
        dest = destination_loader.load_class(config = {'handler': 'anodot_agent.destinations.dummy'})
        self.assertEqual(dest, Destination)

    def test_load_wrong_module(self):
        with self.assertRaises(LoaderError):
            source_loader.load_class(config = {'handler': 'anodot_agent.destinations.dummy'})

if __name__ == '__main__':
    unittest.main()
