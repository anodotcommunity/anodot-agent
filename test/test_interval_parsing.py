import unittest
from anodot_agent.dttm import interval_timedelta
from datetime import timedelta, datetime

class DestinationSendingTestCase(unittest.TestCase):
    def test_load_source(self):
        self.assertEqual(interval_timedelta('3d1h5m'),timedelta(days = 3, hours = 1, minutes = 5))


if __name__ == '__main__':
    unittest.main()
