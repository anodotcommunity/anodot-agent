import unittest
from anodot_agent.lib.simple_json_path.converter import ( 
        SimpleJSONToRecordsConverter, 
        IncompatiblePairError )

class JSONPathCompatibilityTestCase(unittest.TestCase):
    @staticmethod
    def check_compatible_or_raise(jp1,jp2):
        SimpleJSONToRecordsConverter(mapping = {"jp1": jp1, "jp2": jp2})
    
    def test_compatibility(self):
        jp1 = ".data.agents.edges[*].node.nickname"
        jp2 = ".data.agents.edges[*].node"
        self.assertIsNone(self.check_compatible_or_raise(jp1,jp2))
        jp1 = ".data.agents.edges[*].node.summaries[*].threads"
        jp2 = ".data.agents.edges[*].node.summaries[*].time"
        self.assertIsNone(self.check_compatible_or_raise(jp1,jp2))

    def test_incompatible(self):
        with self.assertRaises(IncompatiblePairError):
            jp1 = ".data.agents.edges[*].node.summaries[*].threads"
            jp2 = ".data.agents.edges[*].node.gizmos[*].time"
            self.check_compatible_or_raise(jp1,jp2)
        

class JSONPathConverterTestCase(unittest.TestCase):
    TEST_OBJ = {
        "ver": 20,
        "other_nested": { "nested2": { "nested3": 55 } },
        "nodes": [ 
            { "hostname": "wacky-x", "nickname": "wacky",  "summaries": [ { "time": 1535569740, "threads": 784 }, { "time": 1535569740, "threads": 784, "bottles": 25 } ] },
            { "hostname": "wacky-y", "summaries": [ { "time": 1535569740, "threads": 71, "bottles": 10 }, { "time": 1535569740, "threads": 74, "bottles": 2 } ] },
            ],
        "toys": [ { "a": "1", "b": 123}, {"a": "2"}, {"a":"xyz", "b": 543} ]
        }

    @property
    def one_level_single_prop_converter(self):
        return SimpleJSONToRecordsConverter( mapping = { "a": ".toys[*].a" })
    
    @property
    def one_level_two_prop_converter(self):
        return SimpleJSONToRecordsConverter( mapping = { "a": ".toys[*].a", "b": ".toys[*].b" })

    @property
    def one_level_nested_prop_converter(self):
        return SimpleJSONToRecordsConverter( mapping = { "a": ".toys[*].a", "b": ".toys[*].b", "ver": ".ver" })

#            "hostname": ".nodes[*].hostname",
#            "nickname": ".nodes[*].nickname",
#            "time": ".nodes[*].summaries[*].nickname",

    @property
    def two_level_single_prop_converter(self):
        return SimpleJSONToRecordsConverter( mapping = { "bottles": ".nodes[*].summaries[*].bottles" })
    
    @property
    def two_level_nested_prop_converter(self):
        return SimpleJSONToRecordsConverter( mapping = { "bottles": ".nodes[*].summaries[*].bottles", "hostname": ".nodes[*].hostname", "n3": ".other_nested.nested2.nested3"})

    def test_one_level(self):
        self.assertEqual(self.one_level_single_prop_converter.convert(self.TEST_OBJ),(("a",), [ ("1",),("2",),("xyz",) ]))
        self.assertEqual(self.one_level_two_prop_converter.convert(self.TEST_OBJ),(("a","b"), [("1", 123), ("2", None), ("xyz", 543) ]))
        self.assertEqual(self.one_level_nested_prop_converter.convert(self.TEST_OBJ),(("a","b","ver"), [("1", 123, 20), ("2",None, 20), ("xyz", 543,20)]))

    def test_two_level(self):
        self.assertEqual(self.two_level_single_prop_converter.convert(self.TEST_OBJ),(('bottles',), [( None,),(25,),(10,),(2,)]))
        self.assertEqual(self.two_level_nested_prop_converter.convert(self.TEST_OBJ),(('bottles', 'hostname', 'n3'),
            [
            ( None,  "wacky-x",  55),
            ( 25, "wacky-x",  55),
            ( 10, "wacky-y",  55),
            ( 2, "wacky-y",  55)
            ]))

if __name__ == '__main__':
    unittest.main()

#
#import json
#import yaml
#
#with open('response_example.json','rt') as f:
#    dat = json.load(f)
#
#mapping = yaml.load
#    
#jpri = SimpleJSONToRecordsConverter( mapping = {
#    "nickname": ".data.agents.edges[*].node.nickname",   
#    "hostname": ".data.agents.edges[*].node.hostname",
#    "time": ".data.agents.edges[*].node.summaries[*].time",
#    "threads": ".data.agents.edges[*].node.summaries[*].threads",
#    "fileRead": ".data.agents.edges[*].node.summaries[*].fileRead",
#    "cpu": ".data.agents.edges[*].node.summaries[*].cpu",
#    "fufu": ".data.agents.edges[*].node.summaries[*].fufu",
#})
#
#jpri.get_incompatible_pair()
##jpri.level_map
##jpri.spine
#
##list(jpri.iterate_level(dat))
##dat
#
#
## In[93]:
#
#
#import json

#
#jpri = SimpleJSONToRecordsConverter( mapping = {
#    "ver": ".ver",
#    "time": ".node.summaries[*].time",
#    "threads": ".node.summaries[*].threads",
#    #"blabas": "$.node.summaries[*].threads[*].trala[*].mama",
#    #"shitta": "$.node.shittas[*].fifo",
#    #"xhitta": "$.node.shittas",
#})
#
#jpri.get_incompatible_pair()
#jpri.level_map
#jpri.spine
#
#list(jpri.iterate_level(dat))
#
