Anodot Agent
------------
`anodot-agent` is an agent to continuously send data into Anodot.
The agent is deployed inside a docker container.

Building
--------
``scripts/build.sh`` - builds the docker image. 

Installation
------------

Sources
-------
The agent supports all SQL sources supported by Python 3.6, and in particular:

- MariaDB
- MySQL
- PostgreSQL
- Redshift
- Oracle

Destinations
------------
The agent supports an S3 CSV Destination.

