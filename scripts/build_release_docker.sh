#!/bin/bash
RELEASE=$(pipenv run python -c 'from anodot_agent.version import *;print(FULL_VERSION)')
git archive --prefix=anodot-agent/ HEAD > anodot-agent.tar
docker build . -t anodot/anodot-agent:latest -t anodot/anodot-agent:$RELEASE
