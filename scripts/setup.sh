#!/bin/bash

set -a

SUCCESS='\e[32m[SUCCESS]\e[0m'
CRITICAL='\e[31m[CRITICAL]\e[0m'

#
# General - OS Detection
#

function detectOS () {
    if [ -f /etc/os-release ] ; then
        source /etc/os-release
        echo "${ID}"
    elif [ -f /etc/lsb-release] ; then
        source /etc/lsb-release
        echo "${ID}"
    fi
}


#
# General - exit types
#

function exitToManual () {
    echo -e "\e[33mAutomatic installation failed, please proceed with manual installation"
    echo -e "See the Installation guide at https://www.anodot.com/agent/manual.html\e[0m"
    exit 1
}

function exitSuccess () {
    cat << __EOF__
$SUCCESS Anodot installation complete.

Please manually run the following next steps:

1. Add the following aatool alias to $HOME/.bashrc for easy management:
    echo "alias aatool='docker exec -it anodot-agent aatool'" >> $HOME/.bashrc

2. Log out and re-login ( to make sure your user has permissions to run aatool )

__EOF__
    exit 0
}


#
# Setup - Common
#

function addDockerGroup () {
    grep --quiet ^docker: /etc/group && echo -e "$SUCCESS Docker group exists" || {
        sudo groupadd docker && echo -e "$SUCCESS Added group docker" || {
            echo -e "$CRITICAL error trying to add group docker"
        }
    }
}

function userInDockerGroup () {
    for grp in $(sudo groups "$USER"); do
        if [ "$grp" == "docker" ] ; then return 0; fi
    done
    return 1;
}

function addUserToDockerGroup () {
    INSTALL_USER=$USER
    #    echo -e "Adding the user \`$USER\` to group \`docker\`"
    userInDockerGroup || 
        sudo usermod -a "$USER" -G docker && 
        echo -e "$SUCCESS Added $USER to group docker" ||
        [ ! -z "$(sudo id "$USER" | grep '(docker)')" ] || {
            echo -e "$CRITICAL Failed to add user \`$USER\` to the \`docker\` group" 
            exitToManual
        }
}

function makeAgentHostDir () {
    while true; do
        DEFAULT_DATA_DIR=$HOME/anodot-agent-home
        echo -e "Agent work directory setup"
        read -p "Enter a local work directory for the agent [default - $DEFAULT_DATA_DIR]: " DATA_DIR
        DATA_DIR=${DATA_DIR:-$DEFAULT_DATA_DIR}
        mkdir -p "$DATA_DIR"
        DATA_DIR=$(readlink -f "$DATA_DIR")
        [ -d "$DATA_DIR" ] && echo -e "$SUCCESS Created work directory in \`$DATA_DIR\`" && break
    done
}

function setSELinuxContextHostDir () {
    chcon -t svirt_sandbox_file_t "$DATA_DIR" && echo -e "$SUCCESS Changed SELinux context to allow access from guest docker" || {
        echo -e "$CRITICAL Failed to change SELinux context"
        exitToManual
    }
}

function pullAnodotAgentContainer () {
    sudo su - $USER -c 'docker pull anodot/anodot-agent' &&
    echo -e "$SUCCESS Pulled latest anodot-agent from dockerhub"
}

function runAnodotAgentContainer () {
    sudo su - $USER -c "docker run -d --restart unless-stopped --name anodot-agent -v '$DATA_DIR':/anodot-agent-home anodot/anodot-agent" &&
    echo -e "$SUCCESS anodot-agent is running"
}

function addAliases () {
    echo -e "$SUCCESS"
}

function handleInstallation () {
    addUserToDockerGroup && makeAgentHostDir && pullAnodotAgentContainer && runAnodotAgentContainer && addAliases
}

function checkDockerServiceUp () {
    sudo systemctl is-active --quiet docker && echo -e "$SUCCESS docker service is up" || {
        sudo service docker restart && sleep 2 && sudo systemctl is-active --quiet docker && echo -e "$SUCCESS docker service is up" || {
            echo -e "$CRITICAL could not start docker service" 
            exitToManual    
        }
    }
}

#
# Setup - Ubuntu
#

function handleUbuntuInstallDeps () {
    echo -e "Installing dependencies - root privileges required"

    sudo apt update -y && echo -e "$SUCCESS Updated package list" || { 
        echo -e "$CRITICAL Package update failed - exiting" 
        exitToManual 
    }
    
    sudo apt install -y docker.io && echo -e "$SUCCESS installed docker server" || {
        echo -e "$CRITICAL Docker server installation failed" 
        exitToManual 
    }
}

function handleUbuntu (){
    echo -e "$SUCCESS Detected Platform: Ubuntu"
    handleUbuntuInstallDeps && handleInstallation && exitSuccess
}

#
# Setup - Debian
#

function handleDebianInstallDeps () {
    echo -e "Installing dependencies - root privileges required"

    sudo apt update -y && sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common && echo -e "$SUCCESS installed deps" || {
        echo -e "$CRITICAL Package install failed - exiting" 
        exitToManual 
    }

    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - && echo -e "$SUCCESS added keys to docker repository" || {
        echo -e "$CRITICAL Package install failed - exiting" 
        exitToManual 
    }
    
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && echo -e "$SUCCESS added docker repository" || {
        echo -e "$CRITICAL Package install failed - exiting" 
        exitToManual 
    }

    sudo apt update -y && sudo apt install -y docker-ce && echo -e "$SUCCESS installed docker server" || {
        echo -e "$CRITICAL Docker server installation failed" 
        exitToManual 
    }
}

function handleDebian (){
    echo -e "$SUCCESS Detected Platform: Debian"
    handleDebianInstallDeps && handleInstallation && exitSuccess
}



#
# Redhat / Centos
#

function handleRedhatInstallDeps () {
    sudo yum install --enablerepo=\* -y docker && echo -e "$SUCCESS installed docker server" || {
        echo -e "$CRITICAL Docker server installation failed" 
        exitToManual 
    }
}

function handleRHEL (){
    echo -e "$SUCCESS Detected Platform: Redhat"
    handleRedhatInstallDeps && addDockerGroup && checkDockerServiceUp && handleInstallation && setSELinuxContextHostDir  && exitSuccess
}


#
# Unknown
#

function handleUnknown (){
    echo "Installation platform ( OS / Distribution ) not supported, please set up Docker manually."
    exitToManual
}

#
# main
# 

function main(){
    case "$(detectOS)" in
        "ubuntu") handleUbuntu ;;
	"debian") handleDebian ;;
        "rhel") handleRHEL ;;
        *) handleUnknown ;;
    esac
}

main
