import abc
import csv
import logging
from typing import List, Tuple, Any, TextIO, BinaryIO

logger = logging.getLogger(__name__)

class BaseResultSet(abc.ABC):
    @property
    @abc.abstractmethod
    def columns(self) -> List[str]:
        pass

    @property
    @abc.abstractmethod
    def records(self) -> List[Any]:
        pass

    @property
    @abc.abstractmethod
    def empty(self) -> bool:
        pass


    @property
    @abc.abstractmethod
    def csv(self) -> bytes:
        pass

    @abc.abstractmethod
    def __len__(self) -> int:
        pass

import io

class SimpleResultSet(BaseResultSet):
    def __init__(self, *, columns, records):
        self._columns = columns
        self._records = records

    @property
    def columns(self):
        return self._columns

    @property
    def records(self):
        return self._records

    def __len__(self):
        return len(self._records)

    @property
    def empty(self):
        return bool(self._records)

    @property
    def csv(self) -> bytes:
        outf = io.StringIO()
        wrt = csv.writer(outf)
        wrt.writerow(self.columns)
        wrt.writerows(self.records)
        outf.seek(0)
        return outf.read().encode('utf8')
    
    @property
    def empty(self) -> bool:
        return not self._records

