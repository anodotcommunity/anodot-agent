from abc import ABC, abstractmethod
from typing import Tuple
from ..resultset import BaseResultSet
from datetime import datetime

class BaseScheduler(ABC):
    @abstractmethod
    def should_run(self) -> bool:
        pass

    @abstractmethod
    def update_current_dttm(self, *, dttm: datetime = None) -> None:
        pass

    @abstractmethod
    def get_run_range(self) -> Tuple[datetime,datetime]:
        pass

    @abstractmethod
    def handle_resultset_empty(self) -> None:
        return

    @abstractmethod
    def handle_exception(self, *, err: Exception) -> None:
        pass

    @abstractmethod
    def handle_success(self) -> None:
        pass

    @abstractmethod
    def get_status_summary(self) -> str:
        pass

    @abstractmethod
    def get_next_run_at(self) -> datetime:
        pass


