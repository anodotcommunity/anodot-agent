from .base import BaseScheduler
from datetime import datetime, timedelta
from ..dttm import dttm_ceil, dttm_floor, interval_timedelta
import logging
import enum
import io

logger = logging.getLogger(__name__)

class StreamState(enum.Enum):
    INITIAL         = 0x100
    INITIAL_ERROR   = 0x101
    BACKFILL        = 0x200
    STREAMING       = 0x300
    CATCHUP         = 0x400

class OutOfOrderException(Exception): pass

class SmartScheduler(BaseScheduler):
    r"""SmartScheduler - data-aware scheduling

    a stream has the following state machine: 


                                                success
                                       +------------------------------------+
                                       |                                    |
                                      \|/                                   |
  INITIAL -> BACKFILL -----------> STREAMING ------------------> CATCHUP ---+
     |          /|\    marker in               empty/exception
     |           |      window              
     | error     |                   
     |           | success/empty
     |           | 
     +-----> INITIAL_ERROR


    BACKFILL is the phase for collecting historical data.

    STREAMING is the normal operation - the current query window is the next one after the last successful window. 
                There may be multiple windows ahead that may be queried.

    CATCHUP is when there are windows between the last successful window that will remain empty.

    BACKFILL:
    - empty windows are considered valid resultsets prior to the catchup window.
    - retries are done either indefinitely or until max_retries.

    CATCHUP:
    - Empty / invalid data results are retried indefinitely within the catchup window, which may slide away from empty data.
    - Empty data allows moving forward the queried time window.
    - Errors are assumed to be retry-able, but are blocking the progress of the stream position beyond the catchup window start.

    """ 
    
    def __init__(self, *,   stream_id,
                            current_clock_dttm = None,
                            interval = '1h',
                            delay = '0h',
                            backfill_window = None,             # Backfill - start querying data based on this window
                            catchup_window = None,              # Catchup - if a query returns empty it will be retried within this window.

                            backfill_start_stream_dttm = None,   # start of the backfill
                            
                            retries = 0,
                            retry_delay = None,
                            retry_backoff_base = 2.0,           # retry number n+1 will occur retry_backoff_base^n*retry_delay after retry n
                            retry_delay_max = '6h'):

        # Config params

        self.current_clock_dttm = current_clock_dttm or datetime.utcnow()
        self.interval = interval_timedelta(interval)
        self.delay = interval_timedelta(delay)
        self.backfill_window = interval_timedelta(backfill_window) if backfill_window is not None else self._default_backfill_window
        self.catchup_window = interval_timedelta(catchup_window) if catchup_window is not None else self._default_catchup_window
        
        self.retries = retries
        self.retry_delay = interval_timedelta(retry_delay) if retry_delay is not None else self._default_retry_delay
        self.retry_backoff_base = 2.0
        self.retry_delay_max = interval_timedelta(retry_delay_max)

        # Internal state

        self.stream_state = StreamState.INITIAL
        self.backfill_start_stream_dttm = dttm_floor( dttm = backfill_start_stream_dttm or self._default_start_stream_dttm, delta = self.interval )
        self.retry = 0
            
        self.last_exception              = None
        self.last_exception_stream_dttm  = None
        self.last_exception_clock_dttm   = None
        self.last_successful_stream_dttm = None
        self.last_successful_clock_dttm  = None

        self.current_marker_stream_dttm = self.backfill_start_stream_dttm
        self._wakeup_at_next_queryable_clock_dttm()

    def clear(self):
        self.wakeup_clock_dttm = self.current_clock_dttm
        self.last_exception              = None
        self.last_exception_stream_dttm  = None
        self.last_exception_clock_dttm   = None
        self.last_successful_stream_dttm = None
        self.last_successful_clock_dttm  = None

    def update(self, *, delay = None, retries = None, interval = None, **kwargs):
        if delay is not None:
            self.delay = interval_timedelta(delay)
        if retries is not None:
            self.retries = retries
        if interval is not None:
            self.interval = interval_timedelta(interval)

        # TODO: Add updateable entries, and adapt the schedule state to the update.

    @property
    def _default_retry_delay(self):
        return timedelta(minutes = 5)

    @property
    def _default_backfill_window(self):
        if self.interval >= timedelta(days = 1):
            return timedelta(days = 30)
        else:
            return timedelta(days = 7)
    
    @property
    def _default_catchup_window(self):
        if self.interval >= timedelta(days = 1):
            return timedelta(days = 10)
        else:
            return timedelta(days = 3)

    @property
    def _default_start_stream_dttm(self):
        return self.current_clock_dttm - self.backfill_window - self.delay
 
    def _set_last_exception(self, *, exc = None):
        if exc:
            self.last_exception              = exc
            self.last_exception_stream_dttm  = self.current_marker_stream_dttm
            self.last_exception_clock_dttm   = self.current_clock_dttm
        else:
            self.last_exception = exc
            self.last_exception_stream_dttm = None
            self.last_exception_clock_dttm = None

    def _set_last_success(self):
        self._set_last_exception()
        self.last_successful_stream_dttm = self.current_marker_stream_dttm
        self.last_successful_clock_dttm  = self.current_clock_dttm

    def _set_wakeup_clock_dttm_retry(self):
        self.wakeup_clock_dttm = self.current_clock_dttm + min((self.retry_backoff_base ** self.retry ) * self.retry_delay, self.retry_delay_max)
        logger.debug(f'Next wakeup at {self.wakeup_clock_dttm}')

    def _update_marker_to_successor(self):
        self.current_marker_stream_dttm += self.interval

    def _catchup_window_start_stream_dttm(self):
        return dttm_floor( dttm = self.current_clock_dttm - self.interval - self.delay - self.catchup_window, delta = self.interval )

    def _last_success_successor(self):
        if self.last_successful_stream_dttm is not None:
            return dttm_floor( dttm = self.last_successful_stream_dttm + self.interval, delta = self.interval )

    def _catchup_restart_pos_stream_dttm(self):
        if self._last_success_successor() is not None:
            return max( self._last_success_successor(), self._catchup_window_start_stream_dttm() )
        return self._catchup_window_start_stream_dttm()

    def _update_marker_to_catchup_start(self):
        """sets the next chunk to be the earliest start during catchup"""
        self.current_marker_stream_dttm = self._catchup_restart_pos_stream_dttm()
    
    def _next_queryable_clock_dttm(self, *, stream_dttm = None):
        """return clock time for when the chunk [ dttm, dttm + interval ) can be queried ( "golden" ) based on delay and interval settings """
        return ( stream_dttm or self.current_marker_stream_dttm ) + self.interval + self.delay

    def _last_queryable_stream_dttm(self):
        return dttm_floor( dttm = self.current_clock_dttm - self.interval - self.delay, delta = self.interval )

    def _wakeup_at_next_queryable_clock_dttm(self, *, stream_dttm = None):
        self.wakeup_clock_dttm = self._next_queryable_clock_dttm( stream_dttm = stream_dttm )

    def _wakeup_at_next_interval(self):
        self.wakeup_clock_dttm = dttm_floor( dttm = self.wakeup_clock_dttm, delta = self.interval ) + self.interval

    def _marker_is_last_queryable(self, *, stream_dttm = None):
        return self._next_queryable_clock_dttm( stream_dttm = self.current_marker_stream_dttm + self.interval ) > self.current_clock_dttm

    # State Machine validations

    VALID_TRANSITIONS = {
            (StreamState.INITIAL, StreamState.INITIAL_ERROR),
            (StreamState.INITIAL_ERROR, StreamState.BACKFILL),
            (StreamState.INITIAL, StreamState.BACKFILL),
            (StreamState.BACKFILL, StreamState.CATCHUP),
            (StreamState.BACKFILL, StreamState.STREAMING),
            (StreamState.CATCHUP, StreamState.STREAMING),
            (StreamState.STREAMING, StreamState.CATCHUP)
            }

    def _transition(self, new_stream_state):
        if (self.stream_state, new_stream_state) in self.VALID_TRANSITIONS:
            self.stream_state = new_stream_state
        else:
            logger.warn(f"Illegal stream_state transition blocked: {self.stream_state} --//--> {new_stream_state}")
   
    # BaseScheduler method overrides

    def update_current_dttm(self, *, dttm = None):
        self.current_clock_dttm = dttm or datetime.utcnow()

    def should_run(self):
        if self.current_clock_dttm < self._next_queryable_clock_dttm():
            logger.debug(f"woke up too early: current_clock_dttm = {self.current_clock_dttm}, next_queryable_clock_dttm = {self._next_queryable_clock_dttm()}")
            return False
        return self.wakeup_clock_dttm is None or self.current_clock_dttm > self.wakeup_clock_dttm

    def get_next_run_at(self):
        return self._next_queryable_clock_dttm()

    def get_run_range(self):
        return (self.current_marker_stream_dttm, self.current_marker_stream_dttm + self.interval)

    def handle_start(self, chunk_start_stream_dttm):
        """ sanity check - does not influence state """
        if chunk_start_stream_dttm != self.current_marker_stream_dttm:
            logger.warn(f"started stream on {chunk_start_stream_dttm}, expected {self.current_marker_stream_dttm}")

    def handle_exception(self, *, exc):
        self._set_last_exception(exc = exc)
        logger.info(f"exception {exc} occurred", exc_info = 1)
        self.retry += 1
        self._set_wakeup_clock_dttm_retry()
       
        if self.stream_state == StreamState.INITIAL:
            self._transition(StreamState.INITIAL_ERROR)
        elif self.stream_state == StreamState.INITIAL_ERROR:
            pass
        elif self.stream_state == StreamState.BACKFILL:
            pass
        elif self.stream_state == StreamState.STREAMING:
            if not self._marker_is_last_queryable():
                self._transition(StreamState.CATCHUP)
                self._update_marker_to_catchup_start()
        elif self.stream_state == StreamState.CATCHUP:
            self._update_marker_to_catchup_start()

    def handle_resultset_empty(self):
        self.retry = 0
        if self.stream_state in (StreamState.INITIAL_ERROR, StreamState.INITIAL):
            self._handle_success_common()
            self._transition(StreamState.BACKFILL)
        elif self.stream_state == StreamState.BACKFILL:
            self._handle_success_common()
            if self.current_marker_stream_dttm > self._catchup_window_start_stream_dttm():
                self._transition(StreamState.STREAMING)
        elif self.stream_state == StreamState.STREAMING:
            self._transition(StreamState.CATCHUP)
            self._wakeup_at_next_interval()
        elif self.stream_state == StreamState.CATCHUP:
            if self.current_marker_stream_dttm >= self._last_queryable_stream_dttm(): # actually, it should never be strictly greater
                self._update_marker_to_catchup_start()
                self._wakeup_at_next_interval()
            else:
                self._update_marker_to_successor()
  
    def handle_success(self):
        self._handle_success_common()
        if self.stream_state in (StreamState.INITIAL_ERROR, StreamState.INITIAL):
            self._transition(StreamState.BACKFILL)
        elif self.stream_state == StreamState.BACKFILL:
            if self.current_marker_stream_dttm > self._catchup_window_start_stream_dttm():
                self._transition(StreamState.STREAMING)
        elif self.stream_state == StreamState.STREAMING:
            # normal mode
            pass
        elif self.stream_state == StreamState.CATCHUP:
            self._transition(StreamState.STREAMING)

    def _handle_success_common(self):
        self.retry = 0
        self._set_last_success()
        self._update_marker_to_successor()
        self._wakeup_at_next_queryable_clock_dttm()

    def get_wakeup_clock_dttm(self):
        return self.wakeup_clock_dttm

    def get_status_summary(self):
        retf = io.StringIO()
        o = lambda s: print(s, file = retf)
        if self.last_exception:
            o(f'    Retry #{self.retry}')
            o(f'    Previous run: at {self.last_exception_clock_dttm}')
            o(f'    Exception: {self.last_exception}')
        else:
            o(f'    Previous run: at {self.last_successful_clock_dttm}, SUCCESS.')
        o(f'    Current State: {self.stream_state.name}. Interval: {self.interval}' )
        o(f'    Next run: Time Range [ {self.current_marker_stream_dttm:%F %T}, {self.current_marker_stream_dttm + self.interval:%F %T}) Wakeup at or after {self.wakeup_clock_dttm}')
        retf.seek(0)
        return retf.read()


Scheduler = SmartScheduler



