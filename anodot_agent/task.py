from collections import namedtuple
from typing import NamedTuple, Any, Dict
import logging
import logging.handlers
import queue

class TaskContext(object):
    def __init__(self, *, id, source, destination, scheduler, updated_at ):
        self.id = id
        self.source = source
        self.destination = destination
        self.scheduler = scheduler
        self.updated_at = updated_at
    
    @classmethod
    def from_stream(cls, stream):
        return cls(id = stream.id, source = stream.source, destination = stream.destination, scheduler = stream.scheduler, updated_at = stream.updated_at)

class TaskResult(object):
    def __init__(self, *, id, source, destination, scheduler, prev_updated_at, was_run ):
        self.id = id
        self.source = source
        self.destination = destination
        self.scheduler = scheduler
        self.prev_updated_at = prev_updated_at
        self.was_run = was_run

    def update_stream(self, stream):
        assert stream.id == self.id
        stream.source = self.source
        stream.destination = self.destination
        stream.scheduler = self.scheduler

class StreamTask(object):
    def __init__(self, worker_id: str, job_id: str, ctx: TaskContext, logger: logging.Logger) -> None:
        self.logger = logger
        self.ctx = ctx

    def process(self) -> TaskResult:
        scheduler = self.ctx.scheduler
        scheduler.update_current_dttm()

        source = self.ctx.source
        destination = self.ctx.destination

#        self.logger.debug(f"source: {source.__dict__}")
#        self.logger.debug(f"destination: {destination.__dict__}")
#        self.logger.debug(f"scheduler: {scheduler.__dict__}")

        was_run = False
        if scheduler.should_run():
            was_run = True
            scheduler.update_current_dttm()
            start_dttm, end_dttm = scheduler.get_run_range()
            self.logger.debug(f"run_range: {start_dttm} - {end_dttm}")
            try:
                rs = source.get_chunk(start_dttm = start_dttm, end_dttm = end_dttm)
                self.logger.debug(f"got chunk {rs}")
                if rs.empty: # XXX: this can be replaced with another condition, depending on source
                    scheduler.handle_resultset_empty()
                else:
                    destination.save_chunk(start_dttm = start_dttm, end_dttm = end_dttm, resultset = rs)
                    scheduler.handle_success()
            except Exception as e:
                scheduler.handle_exception(exc = str(e))

        return TaskResult(
                id = self.ctx.id,
                source = self.ctx.source,
                destination = self.ctx.destination,
                scheduler = self.ctx.scheduler,
                prev_updated_at = self.ctx.updated_at,
                was_run = was_run
                )

    @classmethod
    def run(cls, worker_id, work_queue, result_queue):
        job_id = 0
        logger = logging.getLogger(f'{__name__}({worker_id})')
        while True:
            try:
                ctx = work_queue.get()
                task = cls(worker_id, job_id, ctx, logger)
                result = task.process()
                result_queue.put(result)
                job_id += 1
            except queue.Empty:
                time.sleep(0.01)

