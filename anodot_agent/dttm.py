"""anodot_agent.dttm - datetime rounding utils"""
from datetime import datetime, timedelta
from dateutil.tz import tzutc
import re

def dttm_floor(*, dttm: datetime, delta: timedelta, start_dttm: datetime = datetime(1970,1,1)) -> datetime:
    return dttm - ( (dttm - start_dttm) % delta )

def dttm_ceil(*, dttm: datetime, delta: timedelta, start_dttm: datetime = datetime(1970,1,1)) -> datetime:
    return dttm + ( (start_dttm - dttm) % delta )


interval_re = re.compile('^((?P<days>[0-9]+)d)?\s*((?P<hours>[0-9]+)h)?\s*((?P<minutes>[0-9]+)m)?$')

def interval_timedelta(s):
    if not s: 
        raise ValueError('empty string given as interval')
    m = interval_re.match(s)
    if m is None:
        raise ValueError("could not parse string as interval")
   
    g = m.groupdict()

    # print(f"matches for `{s}` are {g}")

    return timedelta(   days = int(g['days'] or '0'), 
                        hours = int(g['hours'] or '0'), 
                        minutes = int(g['minutes'] or'0') )

NAIVE_EPOCH = datetime(1970, 1, 1)
def dttm_from_epoch_seconds(seconds):
    return NAIVE_EPOCH + timedelta( seconds = seconds )
