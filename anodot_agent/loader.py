import importlib
import logging
import abc
from .sources.base import BaseSource
from .destinations.base import BaseDestination
from .schedulers.base import BaseScheduler

logger = logging.getLogger(__name__)

class LoaderError(Exception): pass

class Loader(object):
    def __init__(self, *, loaded_type, exported_var, default = None, check_subclass = None, type_attr = 'handler'):
        self.loaded_type = loaded_type
        self.exported_var = exported_var
        self.default = default
        self.check_subclass = check_subclass
        self.type_attr = type_attr

    def load_class(self, *, config):
        logger = logging.getLogger(__name__ + self.loaded_type)
        modname = config.get(self.type_attr, self.default)
        if modname is None:
            raise LoaderError(f'No `{self.type_attr}` attribute given in the {self.loaded_type} config. ( No Default ) ')

        mod = importlib.import_module(modname)
        if not hasattr(mod, self.exported_var):
            raise LoaderError(f'module {modname} does not expose {self.exported_var} - {self.loaded_type} can not load it')
        
        cls = getattr(mod, self.exported_var)
        if self.check_subclass is not None:
            if not issubclass(cls, self.check_subclass):
                raise LoaderError(f'{self.exported_var} provided in {modname} is not a subclass of {self.check_subclass.__name__} - {self.loaded_type} can not load')
        
        return cls

    def build(self, *, config, **kwargs):
        cls = self.load_class( config = config )
        try:
            return cls(**config, **kwargs)
        except:
            logger.error(f"error initializing {cls}", exc_info = 1)
            raise

source_loader = Loader( loaded_type = 'Source', exported_var = 'Source', check_subclass = BaseSource )
destination_loader = Loader( loaded_type = 'Destination', exported_var = 'Destination', check_subclass = BaseDestination )
scheduler_loader = Loader( loaded_type = 'Scheduler', exported_var = 'Scheduler', default = 'anodot_agent.schedulers.smart', check_subclass = BaseScheduler )
