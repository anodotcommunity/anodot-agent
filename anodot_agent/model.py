from sqlalchemy import Column, DateTime, Interval, PickleType, String, Enum, Boolean, func, text
from .db import Base
import enum
from .loader import source_loader, destination_loader, scheduler_loader

class StreamStatus(enum.Enum):
    ACTIVE_WAITING = 'ACTIVE_WAITING'
    ACTIVE_WORKING = 'ACTIVE_WORKING'
    PAUSED =         'PAUSED'

class Stream(Base):
    __tablename__ = 'streams'
    id              = Column(String(), primary_key = True)
    source          = Column(PickleType())
    destination     = Column(PickleType())
    scheduler       = Column(PickleType())
    status          = Column(Enum(StreamStatus), default = StreamStatus.ACTIVE_WAITING)
    run_at          = Column(DateTime(), default = text('current_timestamp'))
    updated_at      = Column(DateTime(), onupdate=text('current_timestamp'))
