from .config import validate_stream_configs, flatten_stream_configs
from .model import Stream, StreamStatus
from .db import metadata
from .loader import source_loader, destination_loader, scheduler_loader
from datetime import datetime, timedelta
from sqlalchemy import create_engine, select, update, delete
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.attributes import flag_modified
from typing import List
import abc
import anodot_agent.settings as settings
import argparse
import logging
import os
import yaml

logger = logging.getLogger(__name__)

class Command(abc.ABC):
    @classmethod
    def add_subparsers(cls, parser):
        subparsers = parser.add_subparsers(help = 'sub-command help', dest = 'command')

        for cmd_class in cls.__subclasses__():
            subparser = subparsers.add_parser(cmd_class.__command__, help = cmd_class.__help__)
            cmd_class.init_subparser(subparser)

    @classmethod
    def dispatch(cls, args):
        cmd_class = None
        for sc in cls.__subclasses__():
            if sc.__command__ == args.command:
                cmd_class = sc
                break

        logger.debug(f'dispatching {args.command} to new instance of {cmd_class}')
        cmd_instance = cmd_class(**vars(args))
        cmd_instance.run()

class Status(Command):
    __command__ = 'status'
    __help__ = 'Help for status'

    @classmethod
    def init_subparser(cls, subparser):
        subparser.add_argument('--dburl', help = 'DBURL for the agent backend', default = settings.ANODOT_AGENT_DBURL) 

    def __init__(self, *, dburl, **kwargs):
        self.engine = create_engine( dburl )
        metadata.create_all( self.engine )
        self.Session = sessionmaker( self.engine )

    def run(self):
        session = self.Session()
        for stream in session.query(Stream):
            s = stream.scheduler
            next_start_dttm, next_end_dttm = s.get_run_range()
            print(f'Stream [{stream.id}] {stream.status.name}: (last updated {stream.updated_at})')
            print(stream.source.get_status_summary())
            print(stream.destination.get_status_summary())
            print(stream.scheduler.get_status_summary())

class ConfigReaderMixIn:
    @staticmethod
    def yaml_files(d = settings.ANODOT_AGENT_HOME):
        return [ os.path.join(d,f) for f in os.listdir(d) if f.endswith('.yaml') ]
    
    @property
    def stream_configs(self):
        configs = []
        for fn in getattr(self,'files', self.yaml_files()):
            with open(fn,'rt') as f:
                configs.append(yaml.load(f))
        
        new_stream_configs = ([
            stream
                for conf in configs
                    for stream in flatten_stream_configs(conf)
            ])
        
        validate_stream_configs(new_stream_configs)
        return new_stream_configs

class TestSource(Command, ConfigReaderMixIn):
    __command__ = 'test_source'
    __help__ = 'Help for test_source'

    @classmethod
    def init_subparser(cls, subparser):
        subparser.add_argument('stream_ids', nargs = "*", help = 'reset scheduler for existing streams', default = [])

    def __init__(self, *, stream_ids, **kwargs):
        self.stream_ids = stream_ids

    def run(self):
        stream_configs_map = { stream_config['id']: stream_config for stream_config in self.stream_configs }
        for stream_id in self.stream_ids:
            stream_config = stream_configs_map[stream_id]
            source = source_loader.build(config=stream_config['source'], stream_id = stream_id)
            scheduler = scheduler_loader.build(config=stream_config['scheduler'], stream_id = stream_id)
            start_dttm, end_dttm = scheduler.get_run_range()
            logger.info(f"querying the initial range [{start_dttm}, {end_dttm})")
            chunk = source.get_chunk(start_dttm = start_dttm, end_dttm = end_dttm)
            print(chunk.csv.decode())

class LoadConfig(Command, ConfigReaderMixIn):
    __command__ = 'load_config'
    __help__ = 'Help for load_config'
   
    @classmethod
    def init_subparser(cls, subparser):
        subparser.add_argument('files',nargs = '*', help = 'config filenames to load, else all yaml files will be loaded', default = cls.yaml_files())
        subparser.add_argument('--dburl', help = 'DBURL for the agent backend', default = settings.ANODOT_AGENT_DBURL)
        subparser.add_argument('--purge', help = 'remove stream ids which are not provided', default = False, action = 'store_true')
        subparser.add_argument('--reset', help = 'reset scheduler for existing streams', default = [], action = 'append')
        subparser.add_argument('--update-scheduler', help = 'update schedulers in existing streams', default = False, action = 'store_true')

    def __init__(self, *, dburl, files = [], purge, reset, update_scheduler, **kwargs):
        logger.info(f"loading files {files}")
        self.engine = create_engine( dburl )
        metadata.create_all( self.engine )
        self.Session = sessionmaker( self.engine )
        self.files = files
        self.purge = purge
        self.reset = reset
        self.update_scheduler = update_scheduler
   
    
    def update_stream_from_config(self, *, stream, stream_config, new):
        stream_id = stream.id
        stream.source = source_loader.build(config=stream_config['source'], stream_id = stream_id)
        stream.destination = destination_loader.build(config=stream_config['destination'], stream_id = stream_id)
        if new or stream_id in self.reset:
            stream.scheduler = scheduler_loader.build(config=stream_config['scheduler'], stream_id = stream_id)
            if not new:
                logger.info(f'resetting scheduler for [{stream_id}]')
        else:
            if self.update_scheduler:
                stream.scheduler.update(**stream_config['scheduler'])
            stream.scheduler.clear()
            flag_modified(stream, 'scheduler')
            logger.info(f'keeping scheduler for [{stream_id}] in same state ( use --reset {stream_id} to override )')
        stream.status = StreamStatus.ACTIVE_WAITING
        stream.run_at = stream.scheduler.get_next_run_at()

    def streams_to_delete(self, *, stream_config_ids):
        with self.engine.connect() as conn:
            return [ stream_id for stream_id in map(lambda row:row[0], conn.execute(select([Stream.id])).fetchall())
                        if stream_id not in stream_config_ids ]

    def add_or_update_stream_config(self, stream_config):
        logger.debug(f"got stream config {stream_config}")
        stream_id = stream_config['id']
        session = self.Session()
        stream = session.query(Stream).filter_by(id = stream_id).one_or_none()
        
        if stream is None:
            logger.info(f'new stream [{stream_id}]')
            new_stream = Stream( id = stream_id )
            self.update_stream_from_config(stream  = new_stream, stream_config = stream_config, new = True)
            session.add(new_stream)
            session.commit()
        else:
            logger.info(f'stream [{stream_id}] exists, updating')
            self.update_stream_from_config(stream = stream, stream_config = stream_config, new = False)
            session.commit()

    def run(self):
        streams_to_delete = self.streams_to_delete( stream_config_ids = [ s['id'] for s in self.stream_configs ])
        
        if streams_to_delete:
            if self.purge:
                logging.info(f"removing stream_ids due to --purge: {streams_to_delete}")
                with self.engine.connect() as conn:
                    conn.execute(delete(Stream).where(Stream.id.in_(streams_to_delete)))
            else:
                logging.info(f"pausing stream_ids, use --purge to remove missing streams: {streams_to_delete}")
                with self.engine.connect() as conn:
                    conn.execute(
                            update(Stream, values = { "status": StreamStatus.PAUSED } )
                            .where(Stream.id.in_(streams_to_delete)))

        for stream_config in self.stream_configs:
            self.add_or_update_stream_config(stream_config)
    
class CLI(object):
    def main(self):
        parser = argparse.ArgumentParser(prog = 'aatool')
        parser.add_argument('-v','--verbose', action = 'store_true')
        Command.add_subparsers(parser)
        
        args = parser.parse_args()

        logging.basicConfig(level = logging.DEBUG if args.verbose else logging.INFO, format="%(asctime)-15s [%(name)s] %(levelname)s - %(message)s")

        logger.debug(f'command is {args.command}')
        logger.debug(f'args = {args}')
        
        if args.command:
            Command.dispatch(args)
        else:
            parser.print_help()

def main():
    cli = CLI()
    cli.main()

if __name__ == '__main__':
    main()

