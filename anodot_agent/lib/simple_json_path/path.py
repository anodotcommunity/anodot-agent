# coding: utf-8

import re
import typing

MAX_JSONPATH_DEPTH=100

class SimpleJSONPathExpr:
    SELECTOR_RE = None

class FieldSelector(SimpleJSONPathExpr):
    SELECTOR_RE = re.compile(r'[.](?P<field_name>[a-zA-Z_0-9]+)')
    def __init__(self, field_name):
        self.field_name = field_name
    
    def __repr__(self):
        return f'FieldSelector("{self.field_name}")'
    
    def __str__(self):
        return f'.{self.field_name}'
    
    def select(self, obj, with_null = True):
        if type(obj) == dict:
            if self.field_name in obj:
                return obj[self.field_name]
        
        if with_null:
            return None
        
        raise ValueError(f"can't select field {self} from obj {obj}")

    def __eq__(self, other):
        return isinstance(other,FieldSelector) and (other.field_name == self.field_name)
        
class ArraySelector(SimpleJSONPathExpr):
    SELECTOR_RE = re.compile(r'\[\*\]')
    
    def __repr__(self):
        return 'ArraySelector()'
    
    def __str__(self):
        return '[*]'
    
    def __eq__(self, other):
        return isinstance(other,ArraySelector)

    
class SimpleJSONPath:
    """Denotes a relative JSONPath"""
    def __init__(self, path = []):
        self.path = path
        
    def append(self, expr):
        if not isinstance(expr, SimpleJSONPathExpr):
            raise ValueError("append takes only instances of SimpleJSONPathExpr")
        self.path.append(expr)

    @staticmethod
    def consume_expr(s):
        for expr_cls in SimpleJSONPathExpr.__subclasses__():
            m = expr_cls.SELECTOR_RE.match(s)
            if m is not None:
                return expr_cls(**m.groupdict()), s[m.span()[1]:]
        raise ValueError(f"Could not parse SimpleJSONPath fragment {s}")
        
    @classmethod
    def from_string(cls, s):
        path = []
        tail = s
        while tail:
            token, tail = cls.consume_expr(tail)
            path.append(token)
            if len(path) > MAX_JSONPATH_DEPTH:
                raise ValueError(f"JSONPath depth exceeds MAX_JSONPATH_DEPTH={MAX_JSONPATH_DEPTH}")
        return cls(path = path)
    
    def split_by_array_selector(self) -> "typing.Iterator[SimpleJSONPath]":
        path = self.path[:]
        subpath = []
        for expr in path:
            if isinstance(expr, ArraySelector):
                yield SimpleJSONPath(path = subpath)
                subpath = []
            else:
                subpath.append(expr)
        yield SimpleJSONPath(path = subpath)
    
    def select(self, obj):
        ret = obj
        for expr in self.path:
            ret = expr.select(ret)
        return ret
            
    def __repr__(self):
        return f'SimpleJSONPath(path={self.path})'
    
    def __str__(self):
        return ''.join( (( str(expr) for expr in self.path )) )
    
    def __eq__(self, other):
        if isinstance(other, SimpleJSONPath):
            return self.path == other.path
    

