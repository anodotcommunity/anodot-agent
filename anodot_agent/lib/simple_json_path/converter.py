# coding: utf-8
from .path import SimpleJSONPath
from functools import lru_cache
memoize = lru_cache(maxsize = 1)


class IncompatiblePairError(ValueError):
    pass

class SimpleJSONToRecordsConverter:
    """
    This class takes a mapping of (key, jsonpath string or SimpleJSONPath instances)
    """
    def __init__(self, *, mapping):
        self.mapping = {k: v if isinstance(v,SimpleJSONPath) else SimpleJSONPath.from_string(v) 
                            for (k,v) in mapping.items() }
        self.check()
        
    def check(self):
        p = self.get_incompatible_pair()
        if p is not None:
            raise IncompatiblePairError('Incompatible path definitions '
                             f'{{"{p[0]}":"{self.mapping[p[0]]}"}}'
                             ' and '
                             f'{{"{p[1]}":"{self.mapping[p[1]]}"}}')
    
    @staticmethod
    def is_prefix(a,b):
        return a == b[:len(a)]

    @property
    @memoize
    def split_mapping(self):
        return { col: list(jp.split_by_array_selector()) for (col, jp) in self.mapping.items() }

    @property
    @memoize
    def level_map(self):
        ret = {}
        for (col,sjp) in self.split_mapping.items():
            depth = len(sjp) - 1
            if depth not in ret:
                ret[depth] = {}
            ret[depth][col] = sjp
        return ret
    
    @property
    @memoize
    def spine(self):
        return max([sjp[:-1] for sjp in self.split_mapping.values()])
       
    def get_incompatible_pair(self):
        max_item = None
        for elm in self.split_mapping.items():
            if max_item is None:
                max_item = elm
                continue
            if self.is_prefix(max_item[1][:-1], elm[1][:-1]):
                max_item = elm
                continue
            if self.is_prefix(elm[1][:-1], max_item[1][:-1]):
                continue
            return (elm[0], max_item[0])

    
    def build_level_data(self, obj, level):
        cur_level_map = self.level_map.get(level)
        if cur_level_map is None: 
            return {}
        return { col: sjp[level].select(obj) 
                    for (col, sjp) in cur_level_map.items() }           
                       
    def iterate_level(self, obj, level = 0):
        level_data = self.build_level_data(obj, level)
        
        if level < len(self.spine):
            next_level = self.spine[level].select(obj)
            if type(next_level) != list:
                next_level = []

            for subobj in next_level:
                for sublevel_data in self.iterate_level(subobj, level = level + 1):
                    yield {**level_data, **sublevel_data}
        else:
            yield level_data
                

    def convert(self, obj):
        converted_columns = tuple(self.mapping.keys())
        converted_records = [ tuple([ row.get(col) for col in converted_columns ] ) for row in self.iterate_level(obj) ]
        return converted_columns, converted_records
