from abc import ABC, abstractmethod
from ..resultset import BaseResultSet
from datetime import datetime

class SourceConfigurationError(Exception): pass

class BaseSource(ABC):
    @abstractmethod
    def get_chunk(self, *, start_dttm: datetime, end_dttm: datetime) -> BaseResultSet:
        pass

    @abstractmethod
    def check_source(self):
        pass

    @abstractmethod
    def get_status_summary(self):
        pass
