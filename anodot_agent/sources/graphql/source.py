from ..base import BaseSource
from datetime import datetime, timedelta
from ...resultset import SimpleResultSet
from ...templates import render_with_context
from ...dttm import interval_timedelta
from .schema_query import SCHEMA_QUERY
from typing import Dict
import io
import logging
import requests
import graphql
import requests_cache
from .build_converter import build_converter

logger = logging.getLogger(__name__)

class GraphQLSource(BaseSource):
    def __init__(self, *, stream_id, handler, connection: Dict, query: str, graphql_schema_expiration = '1d', **kwargs) -> None:
        self.connection = connection
        self.query = query
        self.graphsql_schema_expiration = interval_timedelta(graphql_schema_expiration)

    def execute(self, query_str, session = requests):
        rsp = session.get(**self.connection, params = { 'query': query_str })
        rsp.raise_for_status()
        return rsp

    def requests_cached_session(self):
        return requests_cache.CachedSession(cache_name = '.graphql_source_http_cache', cache_backend = 'sqlite', expire_after = timedelta(days = 1))

    @property
    def graphql_schema(self):
        with self.requests_cached_session() as session:
            current_time = datetime.utcnow()
            rsp = self.execute(SCHEMA_QUERY, session = session)
            return graphql.build_client_schema(rsp.json()['data'])

    def get_chunk(self, *, start_dttm: datetime, end_dttm: datetime) -> SimpleResultSet:
        rendered_query = render_with_context(
                template_str = self.query,
                start_dttm = start_dttm,
                end_dttm = end_dttm)
        
        logger.debug(f"rendered query template - {rendered_query}")

        doc = graphql.parse(rendered_query)
        logger.debug(f"parsed doc {doc}")
        converter = build_converter(self.graphql_schema, doc)
        
        rsp = self.execute(rendered_query)

        converted_columns, converted_records = converter.convert(rsp.json()['data'])
        return SimpleResultSet(
                    columns = converted_columns,
                    records = converted_records
                    )

    @classmethod
    def from_config(cls, conf):
        return cls(**conf)

    def check_source(self):
        pass
    
    def get_status_summary(self):
        return f'   Source: GraphQLSource, query: {self.query}'

Source = GraphQLSource
