from ...lib.simple_json_path import ( 
        SimpleJSONToRecordsConverter, 
        SimpleJSONPath, FieldSelector, ArraySelector 
        )

import graphql


def _traverse_doc(doc):
    for def_ in doc.definitions:
        for ent in _traverse_selection_set(def_.selection_set):
            yield ent

def _traverse_selection_set(selection_set):
    for sel in selection_set.selections:
        if sel.selection_set is not None:
            for subpath in _traverse_selection_set(sel.selection_set):
                yield (sel.name.value,) + subpath
        else:
            yield (sel.name.value,)

def strip_notnull(field):
    while isinstance(field, graphql.GraphQLNonNull):
        field = field.of_type
    return field

def build_converter(schema, doc):
    mapping = {}
    for doc_path in _traverse_doc(doc):
        json_path = [ ]
        s = schema.get_query_type()
        for field in doc_path:
            while True:
                s = strip_notnull(s)
                if isinstance(s, graphql.GraphQLList):
                    s = s.of_type
                    json_path.append( ArraySelector() )
                    continue
                elif isinstance(s, graphql.GraphQLObjectType):
                    s = s.fields[field].type
                    json_path.append( FieldSelector(field_name = field) )
                    break
                else:
                    raise ValueError(f"Invalid Traversal {s}")

        s = strip_notnull(s)
        if not isinstance(s, graphql.GraphQLScalarType):
            raise ValueError(f"doc_path {doc_path} does not end with a scalar.")
        
        mapping['.'.join(doc_path)] = SimpleJSONPath(json_path)
        if field not in mapping:
            mapping[field] = SimpleJSONPath(json_path)
    
    return SimpleJSONToRecordsConverter(mapping = mapping)


