from .base import BaseSource
from ..resultset import SimpleResultSet
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from jinja2 import Template
import io

class SQLSource(BaseSource):
    def __init__(self, *, stream_id, handler, dburl: str, query: str, **kwargs) -> None:
        self.query = query
        self.dburl = dburl

    @property
    def engine(self):
        return create_engine( self.dburl )

    def get_chunk(self, *, start_dttm: datetime, end_dttm: datetime) -> SimpleResultSet:
        query = (Template(self.query)
                    .render(
                        start_dttm = start_dttm,
                        end_dttm = end_dttm
                        ))
        
       
        with self.engine.connect() as connection:
            sqla_rproxy = connection.execute(query)
            return SimpleResultSet(
                    columns = list(sqla_rproxy.keys()),
                    records = sqla_rproxy.fetchall()
                    )

    @classmethod
    def from_config(cls, conf):
        return cls(**conf)

    def check_source(self):
        pass
    
    def get_status_summary(self):
        retf = io.StringIO()
        dburl = self.engine.url
        print(f'    Source: SQLSource, Host="{dburl.host}" database="{dburl.database}"', file = retf)
        retf.seek(0)
        return retf.read()
    

Source = SQLSource
