from .base import BaseSource
from ..resultset import SimpleResultSet
from typing import List, Tuple, Iterable, Any
from datetime import datetime, timedelta
import logging
import random

logger = logging.getLogger(__name__)

SEED = 15408191

class DummySource(BaseSource):
    """This source generates pseudo-random `Resultset`"""
    def __init__(self, *, properties: List[str], events_per_second: float = 0.1, **kwargs) -> None:
        self.properties = properties
        self.events_per_second = events_per_second

    def chunk_generator(self, *, start_dttm: datetime, end_dttm: datetime) -> Iterable[Tuple]:
        dttm = start_dttm
        rng = random.Random(SEED + start_dttm.toordinal())
        while True:
            dttm += timedelta(seconds = rng.expovariate(self.events_per_second))
            if dttm >= end_dttm: 
                break
            a = (dttm,)
            b = tuple([ rng.uniform(0,1) for a in self.properties ])
            yield a + b  #type: ignore

    def get_chunk(self, *, start_dttm: datetime, end_dttm: datetime) -> SimpleResultSet:
        return SimpleResultSet(
                    columns = ['timestamp'] + self.properties,
                    records = list(self.chunk_generator(start_dttm = start_dttm, end_dttm = end_dttm))
                    )

    def check_source(self):
        logger.debug("check_source called")
        pass

    def get_status_summary(self):
        return "Dummy Status"

# Module API
Source = DummySource
