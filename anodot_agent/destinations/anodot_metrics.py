from .base import BaseDestination
from ..config import ValidationError
from anodot_api.session import AnodotAPISession
from anodot_api.sample import make_sample
from ..dttm import dttm_from_epoch_seconds
import logging
import io

logger = logging.getLogger(__name__)

class AnodotMetricsDestination(BaseDestination):
    def __init__(self, *, handler, stream_id, anodot_api, dttm_column, properties, pivot_metrics = None, fixed_metrics = None):
        assert handler == __name__
        if (pivot_metrics is None ) == (fixed_metrics is None):
            raise ValidationError("exactly one `fixed_metrics` or `pivot_metrics` section required per stream")
        self.stream_id = stream_id
        self.anodot_api = anodot_api
        self.dttm_column = self._validate_dttm_column(dttm_column)
        self.pivot_metrics = self.fixed_metrics = None
        if fixed_metrics is not None:
            self.fixed_metrics = self._validate_fixed_metrics(fixed_metrics)
        else:
            self.pivot_metrics = pivot_metrics
        self.properties = self._validate_properties(properties)

    def session(self):
        return AnodotAPISession(**self.anodot_api)

    def _validate_dttm_column(self, dttm_column):
        if type(dttm_column) != dict:
            raise ValidationError("expected object: dttm_column")
        if "column" not in dttm_column:
            raise ValidationError("dttm_column object must contain a `column` field")
        return dttm_column

    def _validate_fixed_metrics(self, fixed_metrics):
        ret = []
        for (pos, fm) in enumerate(fixed_metrics):
            if "column" not in fm:
                raise ValidationError(f"missing `column` field for fixed metric {fm} in position {pos}")
            ret.append( { "name": fm["column"], **fm } )
        return ret

    def _validate_properties(self, properties):
        ret = []
        for (pos,prop) in enumerate(properties):
            if "column" in prop:
                ret.append({"prop": prop.get("prop", prop["column"]), **prop})
            elif "prop" in prop and "value" in prop:
                ret.append(prop)
            else:
                raise ValidationError("stream [{self.stream_id}]: invalid property configuration {prop} in position {pos}")
        return ret

    def _build_props(self, rec):
        return [ (prop["prop"], prop["value"] if "value" in prop else rec[prop["column"]]) for prop in self.properties ]

    @property
    def _dttm_converter(self):
        
        def _conv_default(rec):
            return rec[self.dttm_column["column"]]

        def _conv_epoch_seconds(rec):
            return dttm_from_epoch_seconds(rec[self.dttm_column["column"]])

        def _conv_epoch_milliseconds(rec):
            return dttm_from_epoch_seconds(rec[self.dttm_column["column"]]/ 1000.0)

        dttm_column_format = self.dttm_column.get('format')

        if dttm_column_format == 'epoch_seconds':
            return _conv_epoch_seconds

        elif dttm_column_format == 'epoch_milliseconds':
            return _conv_epoch_milliseconds
        elif dttm_column_format is None:
            return _conv_default

        raise ValueError(f"Unknown requested dttm_column format: `{dttm_column_format}`")

    def _build_chunks_pivoted(self, resultset):
        dttm_converter = self._dttm_converter

        for row in resultset.records:
            rec = dict(zip(resultset.columns,row))
            for pm in self.pivot_metrics:
                yld = make_sample(
                        dttm = dttm_converter(rec),
                        property_pairs = (
                            [
                                ("what", rec[pm["name_column"]]),
                                ("stream_id", self.stream_id)
                            ] + self._build_props(rec)
                            ),
                        value = rec[pm["value_column"]],
                        )
                logger.info(f"adding metric {yld}")
                yield yld

    def _build_chunks_fixed(self, resultset):
        dttm_converter = self._dttm_converter
        for row in resultset.records:
            rec = dict(zip(resultset.columns,row))
            for fm in self.fixed_metrics:
                yld = make_sample(
                        dttm = dttm_converter(rec),
                        property_pairs = (
                            [
                                ("what", fm["name"]),
                                ("stream_id", self.stream_id)
                            ] + self._build_props(rec)
                            ),
                        value = rec[fm["column"]],
                        )
                yield yld


    def save_chunk(self, *, start_dttm, end_dttm, resultset):
        logger.info(f"resultset contains {len(resultset)} records")
        if self.pivot_metrics != None:
            self.session().post_samples(sorted(self._build_chunks_pivoted(resultset), key = lambda sample: sample["timestamp"]))
        elif self.fixed_metrics != None:
            self.session().post_samples(sorted(self._build_chunks_fixed(resultset), key = lambda sample: sample["timestamp"]))

    def get_status_summary(self):
        retf = io.StringIO()
        print(f'    Destination: Anodot Metrics (Pivoted)"', file = retf)
        retf.seek(0)
        return retf.read()

Destination = AnodotMetricsDestination
