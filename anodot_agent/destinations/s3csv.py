from .base import BaseDestination
import boto3
import logging
import io

logger = logging.getLogger(__name__)

class S3CSVDestination(BaseDestination):
    def __init__(self, *, stream_id, bucket, folder, handler, filename_pattern = '{start_dttm:%Y%m%d%H}.csv', session_args = {}):
        assert handler == __name__
        self.bucket = bucket
        self.folder = folder.strip('/')
        self.session_args = session_args
        self.filename_pattern = filename_pattern

    def session(self):
        return boto3.Session(**self.session_args)

    def save_chunk(self, *, start_dttm, end_dttm, resultset):
        client = self.session().client('s3')
        filename = self.filename_pattern.format( start_dttm = start_dttm )
        key = f'{self.folder}/{filename}'
        data = resultset.csv
        logger.info(f"writing to Bucket {self.bucket}, Key = {key}, {len(data)} Bytes")
        client.put_object(  Bucket = self.bucket,
                            Key = key,
                            Body = resultset.csv )

    def get_status_summary(self):
        retf = io.StringIO()
        print(f'    Destination: CSV on S3, bucket="{self.bucket}" folder="{self.folder}" filename_pattern="{self.filename_pattern}"', file = retf)
        retf.seek(0)
        return retf.read()

Destination = S3CSVDestination
