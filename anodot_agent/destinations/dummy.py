from .base import BaseDestination
import logging

logger = logging.getLogger(__name__)

HEAD_LEN = 5
TAIL_LEN = 5

class DummyDestination(BaseDestination):
    def __init__(self, *, stream_id, handler):
        pass

    def save_chunk(self, *, start_dttm, end_dttm, resultset):
        logger.info(f"save_chunk start_dttm={start_dttm} end_dttm={end_dttm}")
        logger.info(f"resultset columns: {resultset.columns}")
        if len(resultset.records) <= HEAD_LEN + TAIL_LEN:
            for (i,row) in enumerate(resultset.records):
                logger.info(f"records[{i}] = {str(row)})")
        else:
            for (i,row) in enumerate(resultset.records[:HEAD_LEN]):
                logger.info(f"records[{i}] = {str(row)})")
            logger.info("...")
            for (i,row) in enumerate(resultset.records[-TAIL_LEN:]):
                logger.info(f"records[-{TAIL_LEN - i}] = {str(row)})")

Destination = DummyDestination
