from .base import BaseDestination
from ..config import ValidationError
from anodot_api.session import AnodotAPISession
from anodot_api.event import make_event
import logging
import io

logger = logging.getLogger(__name__)

class AnodotEventsDestination(BaseDestination):
    SPECIAL_PROPS = ['description','title','source','category']
    def __init__(self, *, handler, stream_id, anodot_api, dttm_columns, properties):
        assert handler == __name__
        self.stream_id = stream_id
        self.anodot_api = anodot_api
        self.start_dttm_column = self._validate_dttm_column(dttm_columns.get("start"), allow_null = False)
        self.end_dttm_column = self._validate_dttm_column(dttm_columns.get("end"))
        self.properties = self._validate_properties(properties)

    def session(self):
        return AnodotAPISession(**self.anodot_api)

    def _validate_dttm_column(self, dttm_column, allow_null = True):
        if allow_null and dttm_column is None:
            return None
        if type(dttm_column) != dict:
            raise ValidationError("expecting an object for `dttm_column`")
        if "column" not in dttm_column:
            raise ValidationError("missing `column` for dttm_column")
        return dttm_column

    def _validate_properties(self, properties):
        ret = []
        for (pos,prop) in enumerate(properties):
            if "column" in prop:
                ret.append({"prop": prop.get("prop", prop["column"]), **prop})
            elif "prop" in prop and "value" in prop:
                ret.append(prop)
            else:
                raise ValidationError("stream [{self.stream_id}]: invalid property configuration {prop} in position {pos}")
        return ret

    def _build_props(self, rec):
        property_pairs = []
        special_props = {}
        for prop in self.properties:
            prop_name, prop_val = prop["prop"], prop["value"] if "value" in prop else rec[prop["column"]]
            if prop_name in self.SPECIAL_PROPS:
                special_props[prop_name] = prop_val
            else:
                property_pairs.append(prop_name, prop_val)
        
        return special_props, property_pairs

    def _build_chunks(self, resultset):
        for row in resultset.records:
            rec = dict(zip(resultset.columns,row))
            special_props, property_pairs = self._build_props(rec)
            yld = make_event(
                    start_dttm = rec[self.start_dttm_column["column"]],
                    end_dttm = None if self.end_dttm_column is None else rec[self.end_dttm_column["column"]],
                    property_pairs = property_pairs,
                    **special_props
                    )
            logger.info(f"adding metric {yld}")
            yield yld

    def save_chunk(self, *, start_dttm, end_dttm, resultset):
        logger.info(f"resultset contains {len(resultset)} records")
        self.session().post_events(list(self._build_chunks(resultset)))

    def get_status_summary(self):
        retf = io.StringIO()
        print(f'    Destination: Anodot Events (Pivoted)"', file = retf)
        retf.seek(0)
        return retf.read()

Destination = AnodotEventsDestination
