from abc import ABC, abstractmethod

class BaseDestination(ABC):
    @abstractmethod
    def save_chunk(self, *, start_dttm, end_dttm, resultset):
        pass

    @abstractmethod
    def get_status_summary(self):
        pass
