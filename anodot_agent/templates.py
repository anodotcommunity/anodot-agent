from jinja2 import Environment
from .dttm import NAIVE_EPOCH

def filter_epoch(dttm, unit = 'seconds'):
    epoch = (dttm - NAIVE_EPOCH).total_seconds()
    if unit == 'milliseconds':
        return int(epoch * 1000.0)
    return int(epoch)

template_env = Environment()
template_env.filters['epoch'] = filter_epoch

def render_with_context(*, template_str, start_dttm, end_dttm, **kwargs):
    return template_env.from_string(template_str).render(start_dttm = start_dttm, end_dttm = end_dttm, **kwargs)
