STREAM_DEFINITION_KEYS = set(['destination','source','scheduler'])
STREAM_TOPLEVEL_KEYS = STREAM_DEFINITION_KEYS | {'id'}
STREAM_REQUIRED_KEYS = {'id'}

DEFAULTS = {
        'scheduler': { 'handler': 'anodot_agent.schedulers.smart', 'interval': '1h' }
        }

class ValidationError(Exception): pass
class MissingRequiredKeyError(ValidationError): pass
class DuplicateIdError(ValidationError): pass

def flatten_stream_config(stream, file_config):
    ret = {}
    stream_key_set = set(stream.keys())
    for k in STREAM_REQUIRED_KEYS:
        if k not in stream:
            raise MissingRequiredKeyError(f"Stream record does not have required key {k}")

    for k in STREAM_TOPLEVEL_KEYS:
        if not ( k in stream or k in file_config or k in DEFAULTS):
            raise MissingRequiredKeyError(f"Section `{k}` missing for a stream and no default provided")
        if k in stream:
            ret[k] = stream[k]
        elif k in file_config:
            ret[k] = dict(file_config[k])
        elif k in DEFAULTS:
            ret[k] = dict(DEFAULTS[k])

    for k in stream_key_set - STREAM_TOPLEVEL_KEYS:
        if '.' not in k:
            raise ValidationError(f"unknown property {k}")
        prop_path = k.split('.')
        if prop_path[0] not in STREAM_DEFINITION_KEYS:
            raise ValidationError(f"unknown property {prop_path[0]} in nested property {k}")
        if not len(prop_path) == 2:
            raise ValidationError(f"property nesting is allowed only for one level ( {k} )")
        top_level = prop_path[0]

        ret[top_level] = { **ret[top_level], prop_path[1]: stream[k] } # specific overrides general

    return ret

def flatten_stream_configs(file_config):
    for stream in file_config['streams']:
        yield flatten_stream_config(stream, file_config)

def validate_stream_configs_unique_ids(stream_configs):
    ids = [ s['id'] for s in stream_configs ]
    seen = set()
    dupes = set()
    for i in ids:
        if i in seen: dupes.add(i)
        seen.add(i)

    if dupes:
        raise DuplicateIdError(dupes)

def validate_stream_configs(stream_configs):
    validate_stream_configs_unique_ids(stream_configs)

