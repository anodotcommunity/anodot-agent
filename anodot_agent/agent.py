from .db import metadata
from .model import Stream, StreamStatus
from .task import StreamTask, TaskContext
from multiprocessing import Process, Queue
import queue
import sqlalchemy as SA
from sqlalchemy.orm import sessionmaker
import logging
import random
import time
from datetime import datetime

logger = logging.getLogger(__name__)
logging.getLogger('botocore').setLevel(logging.WARN)

class Agent(object):
    def __init__(self, *,
                    home,
                    dburl,
                    processes,
                    poll_delay_msecs
                    ):

        logger.info(f'dburl = {dburl}')

        self.home = home
        self.dburl = dburl
        self.processes = processes
        self.poll_delay_msecs = poll_delay_msecs

        self.engine = SA.create_engine( self.dburl )

    def start(self):
        self.initialize()
        self.start_workers()
        self.loop()

    def initialize(self):
        metadata.create_all( bind = self.engine )
        Session = sessionmaker(bind = self.engine)
        self.session = Session()
        self.work_queue = Queue()
        self.result_queue = Queue()

        self.session.execute(SA.update(Stream.__table__, values = { Stream.status: StreamStatus.ACTIVE_WAITING } ).where(Stream.status == StreamStatus.ACTIVE_WORKING))
        self.session.commit()

    def build_worker(self):
        worker_id = 'StreamWorker-' + ''.join(random.choices('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', k = 4))
        return Process( target = StreamTask.run,
                        args = (worker_id, self.work_queue, self.result_queue ) )

    def start_workers(self):
        self._workers = [ self.build_worker() for i in range(self.processes) ]
        for w in self._workers:
            w.start()

    def poll_stream(self):
        return (self.session
                .query(Stream)
                .filter_by(status = StreamStatus.ACTIVE_WAITING)
                .filter(Stream.run_at <= datetime.utcnow())
                .order_by(Stream.run_at)
                .limit(1)
                ).one_or_none()

    def put_new_stream(self):
        stream = self.poll_stream()
        task_id = ''.join(random.choices('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', k = 10))
        if stream is not None:
            #logger.debug(f'stream {stream.id} fetched, {stream and stream.scheduler.__dict__}')
            stream.status = StreamStatus.ACTIVE_WORKING
            self.session.commit()
            self.work_queue.put(TaskContext.from_stream(stream)) # assuming infinite queue
            return True

    def handle_new_result(self):
        try:
            res = self.result_queue.get_nowait()
            stream = (self.session
                    .query(Stream)
                    .filter_by(id = res.id, status = StreamStatus.ACTIVE_WORKING) #updated_at = res.prev_updated_at)
                    ).one_or_none()
            if stream is not None:
                logger.debug(f'stream [{res.id}] status={stream.status} run_at={stream.run_at} updated_at={stream.updated_at} prev_updated_at={res.prev_updated_at} ')
                if stream.updated_at != res.prev_updated_at:
                    logger.debug(f"stream [{stream.id}] got changed during task run - not updating")
                    self.session.rollback()
                    return True
                res.update_stream(stream)
                stream.status = StreamStatus.ACTIVE_WAITING
                stream.run_at = stream.scheduler.get_next_run_at()
            else:
                logger.debug(f'stream [{res.id}] got changed during task run - not updating')
            self.session.commit()
            if res.was_run:
                return True
        except queue.Empty:
            self.session.commit()

    def loop(self):
        delay = self.poll_delay_msecs / 1000.0
        while True:
            no_activity_counter = 0
            while not(self.put_new_stream() or self.handle_new_result()):
                if int( (no_activity_counter * delay ) ** 0.4 ) < int( ( (no_activity_counter +1) *delay ) **0.4 ):
                    logger.debug(f'polling every {self.poll_delay_msecs} msecs - no events for {1+no_activity_counter} iterations')
                no_activity_counter += 1
                time.sleep(delay)

def main():
    import anodot_agent.settings as S

    agent = Agent(
                home = S.ANODOT_AGENT_HOME,
                dburl = S.ANODOT_AGENT_DBURL,
                processes = S.ANODOT_AGENT_WORKERS,
                poll_delay_msecs = S.ANODOT_AGENT_POLL_DELAY_MSEC
                )
    agent.start()

if __name__ == '__main__':
    import logging
    logging.basicConfig( level = logging.DEBUG, format = '%(asctime)-15s [%(name)s] %(levelname)s - %(message)s' )
    main()
