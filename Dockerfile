FROM alpine:3.7
COPY requirements.txt /
RUN apk update --no-cache && \
    : && \
    apk add --no-cache --update bash python3 curl coreutils ca-certificates openssl mysql-dev postgresql-dev gcc libc-dev python3-dev libpq && \
    : && \
    update-ca-certificates && \
    : && \
    pip3 install --upgrade pip && \
    : && \
    pip3 install -r /requirements.txt && \
    : && \
    apk del --no-cache libc-dev gcc postgresql-dev mysql-dev
RUN apk add --no-cache mariadb-client-libs
ADD anodot-agent.tar /
RUN \
    pip install https://bitbucket.org/anodotcommunity/anodot-api-python/get/296b8e3e8cb7.tar.bz2 && \
    ( cd /anodot-agent/ && python3 setup.py install ) && \
    mkdir /anodot-agent-home
ENTRYPOINT docker_entrypoint.sh
