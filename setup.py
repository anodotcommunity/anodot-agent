#!/usr/bin/python
from setuptools import setup, find_packages
import os
import sys
import re


def find_version():
    version_file_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),'anodot_agent','version.py')
    globals_ = {}
    locals_ = {}
    exec(compile(open(version_file_path,'rb').read(), version_file_path, 'exec'), globals_, locals_)
    return locals_['FULL_VERSION']

if __name__ == '__main__':
    setup(name='anodot-agent',
        version = find_version(),
        description = 'Anodot Agent Collector',
        author = 'Anodot',
        author_email = 'moran.cohen+anodot-agent@anodot.com',
        url = 'https://www.anodot.com',
        packages = find_packages(),
        scripts = [ 'scripts/docker_entrypoint.sh' ],
        entry_points = {
            'console_scripts': [
                'anodot-agent = anodot_agent.service:main',
                'aatool = anodot_agent.cli:main',
            ]},
    )

